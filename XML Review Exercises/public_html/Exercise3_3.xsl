<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : Exercise3.xsl
    Created on : 13 de febrero de 2019, 13:44
    Author     : dam1
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Exercise3</title>
            </head>
            <body>
                <table border="1">
                    <tr bgcolor="grey" align="center">
                        <th>Name</th>
                        <th>Detail</th>
                        <th>Price</th>
                        <th>Order</th>
                        <th>Reference</th>
                    </tr>
                    <xsl:for-each select="Articles/Article">
                        <xsl:if test="Name = 'T-shirt'">
                            <tr>
                                <td>
                                    <xsl:value-of select="Name"/>
                                </td>
                                <td>
                                    <xsl:value-of select="Detail"/>
                                </td>
                                <td>
                                    <xsl:value-of select="Order"/>
                                </td>
                                <td>
                                    <xsl:value-of select="Price"/>
                                </td>
                                <td>
                                    <xsl:value-of select="Ref"/>
                                </td>
                            </tr>
                        </xsl:if>
                        <xsl:if test="Name = 'Accessories'">
                            <tr>
                                <td>
                                    <xsl:value-of select="Name"/>
                                </td>
                                <td>
                                    <xsl:value-of select="Detail"/>
                                </td>
                                <td>
                                    <xsl:value-of select="Order"/>
                                </td>
                                <td>
                                    <xsl:value-of select="Price"/>
                                </td>
                                <td>
                                    <xsl:value-of select="Ref"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
