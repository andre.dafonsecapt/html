<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : Exercise3.xsl
    Created on : 13 de febrero de 2019, 13:44
    Author     : dam1
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Exercise3</title>
            </head>
            <body>
                <table border="1">
                    <tr bgcolor="grey" align="center">
                        <th>Detail</th>
                        <th>Price (No Tax</th>
                        <th>Price (With Tax</th>
                    </tr>
                    <xsl:for-each select="Articles/Article">
                        <tr>
                            <td>
                                <xsl:value-of select="Detail"/>
                            </td>
                            <td>
                                <xsl:value-of select="Price"/>
                            </td>
                            <td>
                                <xsl:value-of select="Price * 1.21"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
