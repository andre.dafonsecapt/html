<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : courses.xsl
    Created on : February 20, 2019, 7:44 PM
    Author     : andre
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>List of Courses</title>
                <style>
                    table {
                    border-collapse: separate;
                    background:#fff;
                    border-radius: 10px;
                    margin:50px auto;
                    box-shadow:0px 0px 5px rgba(0,0,0,0.3);
                    }
                    th{
                    font-family: 'Patua One', cursive;
                    font-size:23px;
                    font-weight:400;
                    color:#fff;
                    text-shadow:1px 1px 0px rgba(0,0,0,0.5);
                    text-align:center;
                    padding:20px;
                    background-image:linear-gradient(#f4413b, #4a5564);
                    border-top:1px solid #858d99; 
                    }
                    tbody tr td {
                    font-family: 'Open Sans', sans-serif;
                    font-weight:400;
                    color:#5f6062;
                    font-size:13px;
                    padding:20px 20px 20px 20px;
                    border-bottom:1px solid #e0e0e0;
                    }
                    tbody tr:nth-child(2n) {
                    background:#f0f3f5;
                    }
                </style>
            </head>
            <body>
                <h1 align="center">List of Courses</h1>
                <xsl:apply-templates select="Courses/Course"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="Courses/Course">
        <h2 align="center">
            <xsl:value-of select="@name"/>
        </h2>
        <table border="1">
            <tr bgcolor="grey">
                <th>Module</th>
                <th>Code</th>
                <th>Credits</th>
                <th>Price</th>
                <th>Exam Slot</th>
                <th>Teacher</th>
            </tr>
            <xsl:for-each select="Module">
                <tr>
                    <td>
                        <xsl:value-of select="Name"/> (Year <xsl:value-of select="@year"/>)
                    </td>
                    <td>
                        <xsl:value-of select="Code"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="Credits"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="Price"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="ExamSlot"/>
                    </td>
                    <td>
                        <xsl:value-of select="Teacher"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>