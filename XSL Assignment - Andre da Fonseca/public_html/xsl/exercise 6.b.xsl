<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : exercise 5.xsl
    Created on : 7 de febrero de 2019, 12:45
    Author     : dam1
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>BookShop</title>
            </head>
            <body>
                <xsl:for-each select="bookShop/book/section">
                    <h2>Section</h2>
                    <table>
                        <tr>
                            <th>Title</th>
                            <th>Nº Pages</th>
                        </tr>
                        <xsl:for-each select="chapter">
                            <tr>
                                <td>
                                    <xsl:value-of select="title"/>
                                </td>
                                <td>
                                    <xsl:value-of select="npages"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </table>
                </xsl:for-each>                       
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
