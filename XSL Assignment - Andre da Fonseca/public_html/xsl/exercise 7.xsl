<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : exercise 7.xsl
    Created on : 7 de febrero de 2019, 13:19
    Author     : dam1
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Exercise 7</title>
            </head>
            <body>
                <h2 align="center">
                    <xsl:value-of select="PAGE/HEADING"/>
                </h2>
                <div style="float:left;">
                    <xsl:for-each select="PAGE/ARTICLE">
                        <h3>
                            <xsl:value-of select="TITLE"/>
                        </h3>
                        <p>
                            <xsl:value-of select="DESCRIPTION"/>
                        </p>
                    </xsl:for-each>  
                </div>
                <div align="right">
                    <xsl:for-each select="PAGE/ASIDE">
                        <h3>
                            <xsl:value-of select="TITLE"/>
                        </h3>
                        <p>
                            <xsl:value-of select="ITEM"/>
                        </p>
                    </xsl:for-each>
                </div>
                <h2 align="center">
                    <xsl:value-of select="PAGE/FOOTER"/>
                </h2>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
