<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : food.xsl
    Created on : 30 de enero de 2019, 13:38
    Author     : dam1
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Food</title>
            </head>
            <body>
                <table>
                    <tr bgcolor="yellow">
                        <th>Food Item</th>
                        <th>Carbs (g)</th>
                        <th>Fiber (g)</th>
                        <th>Fat (g)</th>
                        <th>Energy (kj)</th>
                    </tr>
                    <xsl:for-each select="food_list/food_item">
                        <tr bgcolor="green">
                            <td>
                                <xsl:value-of select="name"/>
                            </td>
                            <td>
                                <xsl:value-of select="carbs_per_serving"/>
                            </td>
                            <td>
                                <xsl:value-of select="fiber_per_serving"/>
                            </td>
                            <td>
                                <xsl:value-of select="fat_per_serving"/>
                            </td>
                            <td>
                                <xsl:value-of select="kj_per_serving"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
