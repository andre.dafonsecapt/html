<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : exercise 5.xsl
    Created on : 7 de febrero de 2019, 12:45
    Author     : dam1
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>BookShop</title>
            </head>
            <body>
                <h2>Book Titles:</h2>
                <ul>
                    <xsl:for-each select="bookShop/book">
                        <li><xsl:value-of select="title"/></li>
                    </xsl:for-each>                       
                </ul>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
