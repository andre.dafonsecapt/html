<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : Example 1 XSLT.xsl
    Created on : 30 de enero de 2019, 12:57
    Author     : dam1
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title></title>
            </head>
            <body>
                <h2>My CD Collection</h2>
                <ol>
                    <xsl:for-each select="catalog/cd">
                        <li>
                            <xsl:value-of select="title" /> by <xsl:value-of select="artist" />
                        </li>
                    </xsl:for-each>
                </ol>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
