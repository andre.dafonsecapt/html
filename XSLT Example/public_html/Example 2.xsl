<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : Example 2.xsl
    Created on : 30 de enero de 2019, 13:28
    Author     : dam1
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Example 2.xsl</title>
            </head>
            <body>
                <h2>My CD Collection</h2>
                <ol>
                        <xsl:apply-templates/>
                </ol>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="cd">
        <li>
            <xsl:apply-templates select="title"/> by <xsl:apply-templates select="artist"/>
        </li>
    </xsl:template>

    <xsl:template match="title">
        <xsl:value-of select="."/>
    </xsl:template>
    
    <xsl:template match="artist">
        <xsl:value-of select="."/>
    </xsl:template>
    
</xsl:stylesheet>
