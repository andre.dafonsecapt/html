<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : exercicio 3.xsl
    Created on : February 28, 2019, 12:30 AM
    Author     : andre
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Petshop_web</title>
            </head>
            <body>
                <h1>Petshop Carola</h1>
                <ol>
                    <xsl:for-each select="pets/pet">
                        <li>
                            <h2>
                                <xsl:value-of select="name"/>
                            </h2>
                            <ul>
                                <li>Class: <xsl:value-of select="class"/></li>
                                <li>Breed: <xsl:value-of select="breed"/></li>
                                <li>Hair: <xsl:value-of select="hair"/></li>
                                <li>Sex: <xsl:value-of select="sex"/></li>
                                <li>Date of birth: <xsl:value-of select="dob"/></li>
                                <li>Adresses:
                                    <ol> 
                                        <xsl:apply-templates select="addresses"/>
                                    </ol>
                                </li>
                                <li>Telephone: <xsl:value-of select="telefone"/></li>   
                                
                                <li>Sex: <xsl:value-of select="sex"/></li>  
                            </ul>
                        </li>
                    </xsl:for-each>
                </ol>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="addresses/address">
        <li>
            <xsl:value-of select="@type"/>'s Adress: 
            <xsl:value-of select="street"/>,<xsl:value-of select="number"/>,
            <xsl:value-of select="floor"/>,<xsl:value-of select="zip"/>,
            <xsl:value-of select="city"/>
        </li>   
    </xsl:template>
</xsl:stylesheet>


