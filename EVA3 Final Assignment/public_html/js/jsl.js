// this function adds the event listener 'click' to the button 'Create Minefield'
function assignEvents(e) {
    document.getElementById("button").addEventListener("click", startGame);
}
// this function disables the inputs for the game parameters and calls the function 
// drawMinefield with the width and height parameters. It also starts the interval
// of a second for the game clock.
function startGame() {
    time = 0;
    document.getElementById("time").textContent = "Seconds: " + time;
    document.getElementById("message").textContent = "Playing...";
    document.getElementById("fieldset").disabled = true;
    width = document.getElementById("rows").value;
    height = document.getElementById("columns").value;
    drawMinefield();
    interval = setInterval("increment()", 1000);
}
//this function creates a table with the width and height parameters and adds 
//to each cell of the table a button. It also calculates the number of mines to
//be placed and calls the function fillMinefield.
function drawMinefield() {
    var table = document.getElementById("table");
    for (i = 0; i < width; i++) {
        tr = document.createElement("tr");
        table.appendChild(tr);
        for (j = 0; j < height; j++) {
            td = document.createElement("td");
            tr.appendChild(td);
            var button = document.createElement("input");
            button.setAttribute("type", "button");
            button.id = i + "-" + j;
            button.name = "cellButton";
            button.style.height = "25px";
            button.style.width = "25px";
            td.appendChild(button);
        }
    }
    minesNumber = Math.round((document.getElementById("rows").value * document.getElementById("columns").value * document.getElementById("level").value) / 15);
    minesToFind = minesNumber;
    document.getElementById("bombs").textContent += " " + minesNumber;
    fillMinefield(minesNumber);
}
//this function creates a double array with numeric values and places the mines
//randomly in this grid with a value of '*'. Then it calculates the value for 
//each cell in this double array, for each adjacent 'mine' it increments the value
//by 1. It also adds an event listener 'mousedown' for each button in the table.
function fillMinefield(minesNumber) {
    grid = [];
    for (var i = 0; i < width; i++) {
        grid[i] = [];
        for (var j = 0; j < height; j++) {
            grid[i][j] = 0;
        }
    }
    for (var i = 0; i < minesNumber; i++) {
        do {
            var posX = Math.round(Math.random() * (height - 1));
            var posY = Math.round(Math.random() * (width - 1));
        } while (grid[posX][posY] === "*");
        grid[posX][posY] = "*";
        for (var ii = posX - 1; ii <= posX + 1; ii++) {
            for (var jj = posY - 1; jj <= posY + 1; jj++) {
                if (ii >= 0 && jj >= 0 && ii < height && jj < width && grid[ii][jj] !== "*") {
                    grid[ii][jj] += 1;
                }
            }
        }
    }

    var btn = document.getElementsByName("cellButton");
    for (var i = 0; i < btn.length; i++) {
        btn[i].addEventListener("mousedown", cellClicked);
    }
}
//this function is called when a button is pressed down with a mouse button. If
//this was a left mouse click and if the value of the button isn't a ' ' 
//(button with flag), it will call the function 'showCellValue' if the
//correspondent value of the double array isn't a '*', otherwise you lose the game.
//Pressing down on the right mouse button will call the function 'flagMine'
function cellClicked() {
    var btnNode = document.getElementById(event.target.id);
    var idCellString = event.target.id;
    var cellValue = grid[idCellString.substring(0, 1)][idCellString.substring(2, 3)];
    if (event.which === 1) {
        if (btnNode.value !== " ") {
            if (cellValue === "*") {
                document.getElementById("message").textContent = "OH NO!! You Lost!";
                document.getElementById("message").style.color = "red";
                clearInterval(interval);
                for (var i = 0; i < width; i++) {
                    for (var j = 0; j < height; j++) {
                        var btnNode = document.getElementById(i + "-" + j);
                        var idCellString = i + "-" + j;
                        var cellValue = grid[idCellString.substring(0, 1)][idCellString.substring(2, 3)];
                        if (cellValue === "*") {
                            btnNode.style.background = "url('images/Mine_RedBack.png')";
                        }
                        btnNode.disabled = true;
                    }
                }
            } else {
                showCellValue(btnNode, cellValue, idCellString);
            }
        }
    } else {
        flagMine(btnNode, cellValue);
    }
}
//this function displays the value of the double array correspondent with the
//table button pressed if the button pressed doesn't have a value of ' ' 
//(button with flag). If this value is a '0' the function will call itself
//again until the value is no longer a '0'. It also calls the function 'checkWin'
//every time.
function showCellValue(btnNode, cellValue, idCellString) {
    if (btnNode.value !== " ") {
        if (cellValue !== 0) {
            btnNode.value = cellValue;
            btnNode.disabled = true;
        } else if (btnNode.disabled === false) {
            btnNode.value = cellValue;
            btnNode.disabled = true;
            var posX = parseInt(idCellString.substring(0, 1));
            var posY = parseInt(idCellString.substring(2, 3));
            for (var i = posX - 1; i <= posX + 1; i++) {
                for (var j = posY - 1; j <= posY + 1; j++) {
                    if (i >= 0 && j >= 0 && i < height && j < width) {
                        if (!(i === posX && j === posY)) {
                            var newBtnNode = document.getElementById(i + "-" + j);
                            var newIdCellString = i + "-" + j;
                            var newCellValue = grid[newIdCellString.substring(0, 1)][newIdCellString.substring(2, 3)];
                            newBtnNode.value = newCellValue;
                            if (newCellValue === 0) {
                                showCellValue(newBtnNode, newCellValue, newIdCellString);
                            }
                            newBtnNode.disabled = true;
                        }
                    }
                }
            }
        }
        checkWin();
    }
}
//this function places the image of a flag and a value of ' ' on the button pressed with a right
//click. If the button as a value of ' ' it will remove the image and restore 
//the button value. It also calls the function 'checkWin' if the variable 
//'minesToFind' is equal to '0'. Also it subtracts '1' from 'minesToFind' when a
//flag is placed on a button with a 'mine' and adds it when removing a flag.
function flagMine(btnNode, cellValue) {
    if (btnNode.value !== " ") {
        btnNode.style.background = "url('images/Flag.png')";
        btnNode.value = " ";
        minesNumber -= 1;
        document.getElementById("bombs").textContent = "Bombs: " + minesNumber;
        if (cellValue === "*") {
            minesToFind--;
            if (minesToFind === 0) {
                checkWin();
            }
        }
    } else {
        minesNumber += 1;
        document.getElementById("bombs").textContent = "Bombs: " + minesNumber;
        btnNode.style.background = "";
        btnNode.value = "";
        if (cellValue === "*") {
            minesToFind++;
        }
    }
}
//this function checks if you have won the game, it checks if all the buttons
//that don´t have a mine are disabled. If won it will display a message and
//place an image of a mine on the buttons with mines.
function checkWin() {
    var win = true;
    for (var i = 0; i < width; i++) {
        for (var j = 0; j < height; j++) {
            var btnNode = document.getElementById(i + "-" + j);
            var idCellString = i + "-" + j;
            var cellValue = grid[idCellString.substring(0, 1)][idCellString.substring(2, 3)];
            if (cellValue !== "*" && btnNode.disabled === false) {
                win = false;
            }
        }
    }
    if (win) {
        document.getElementById("bombs").textContent = "Bombs: 0";
        document.getElementById("message").textContent = "Good job!!! You won";
        document.getElementById("message").style.color = "green";
        clearInterval(interval);
        for (var i = 0; i < width; i++) {
            for (var j = 0; j < height; j++) {
                var btnNode = document.getElementById(i + "-" + j);
                var idCellString = i + "-" + j;
                var cellValue = grid[idCellString.substring(0, 1)][idCellString.substring(2, 3)];
                if (cellValue === "*") {
                    btnNode.style.background = "url('images/Mine.png')";
                }
                btnNode.disabled = true;
            }
        }
    }
}
//this function increments the variable 'time' by 1 each second that passes.
function increment() {
    time += 1;
    document.getElementById("time").textContent = "Seconds: " + time;
}

document.addEventListener("DOMContentLoaded", assignEvents);

