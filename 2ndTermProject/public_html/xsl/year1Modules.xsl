<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : courses.xsl
    Created on : February 20, 2019, 7:44 PM
    Author     : andre
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>List of Courses</title>
            </head>
            <body>
                <h1>List of Courses</h1>
                <xsl:apply-templates select="Courses/Course"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="Courses/Course">
        <h2>
            <xsl:value-of select="@name"/>
        </h2>
        <table border="1">
            <tr bgcolor="grey">
                <th>Module</th>
                <th>Code</th>
                <th>Credits</th>
                <th>Price</th>
                <th>Exam Slot</th>
                <th>Teacher</th>
            </tr>
            <xsl:for-each select="Module">
                <tr>
                    <td>
                        <xsl:value-of select="Name"/>
                    </td>
                    <td>
                        <xsl:value-of select="Code"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="Credits"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="Price"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="ExamSlot"/>
                    </td>
                    <td>
                        <xsl:value-of select="Teacher"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
</xsl:stylesheet>