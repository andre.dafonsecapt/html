<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : students.xsl
    Created on : February 20, 2019, 7:41 PM
    Author     : andre
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>List of Students</title>
            </head>
            <body>
                <h1>List of Tutored Students</h1>
                <table border="1">
                    <tr bgcolor="grey">
                        <th>Name</th>
                        <th>Age</th>
                        <th>Tutored</th>
                        <th>Home City</th>
                        <th>Major</th>
                        <th>Modules</th>
                    </tr>
                    <xsl:for-each select="students/student">
                        <xsl:if test="@tutored='y'">
                            <tr>
                                <td>
                                    <xsl:value-of select="name"/>
                                </td>
                                <td>
                                    <xsl:value-of select="@age"/>
                                </td>
                                <td align="center">
                                    <xsl:value-of select="@tutored"/>
                                </td>
                                <td>
                                    <xsl:value-of select="address/city"/>
                                </td>
                                <td>
                                    <xsl:apply-templates select="major"/>
                                </td>
                                <td>
                                    <xsl:apply-templates select="results/result"/>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="major">
        <xsl:value-of select="."/>
        <br/>
    </xsl:template>
    
    <xsl:template match="results/result">
        <xsl:value-of select="@module"/>
        <br/>
    </xsl:template>


</xsl:stylesheet>
